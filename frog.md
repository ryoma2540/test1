# Sphinx編集

Sphinxの記述方法には、MarkdownとreStructuredTextの2種類があります。

## Markdown

Sphinxでは、設定を追加することでマークダウンでHTMLを作成することができます。<br>
マークダウンとは、QiitaやGit等でも使用する、書きやすく、読みやすいテキストをHTMLに変換するフォーマットです。<br>
マークダウンのファイルは「.md」の拡張子を持つファイルで、Visual Studio Code等のエディターではプレビュー機能も実装されており、普及しています。<br>
調べると書き方やその例が多く出てきます。<br>

<img src="markdown.png"><br>

- 長所：
    1. 読みやすい
    2. 書きやすい
    3. 普及している
    4. HTMLの記述が使える
    - 短所：
        1. 表現が限られている

***

## reStructuredText

reStructuredTextとは、SphinxにおけるデフォルトのテキストをHTMLに変換するフォーマットです。<br>
Sphinx独自のフォーマットと考えても問題はありません。<br>
reStructuredTextのファイルは「.rst」の拡張子を持つファイルで、Visual Studio Codeでは、拡張機能を入れることでプレビュー機能を使用することができるみたいです。<br>
あまり普及しておらず、調べてもほとんどSphinxの公式サイトに記載されていることしか出てきません。<br>

<img src="rst.png"><br>

- 長所：
    1. 表現が多彩
    2. 読みやすい
    3. 普及している
    - 短所：
        1. そこまで普及していない
        2. 理解するまで時間がかかる
        3. HTMLの記述が使えない